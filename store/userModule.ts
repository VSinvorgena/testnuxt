interface IDictionary<TValue> {
  [id: number]: TValue;
}

interface ItemDictionary {
  [id: number]: Item;
}

interface Rating {
  rete: number,
  count: number
}

interface Item {
  title: string,
  price: number,
  description: string,
  category: string,
  image: string,
  rating: Rating
}

import {VuexModule, Module, Mutation, Action} from 'vuex-class-modules';

@Module
export class UserModule extends VuexModule {
  items = {} as any
  basket = {} as IDictionary<number>

  @Mutation
  setItems(items: any) {
    this.items = {...items}
  }
  @Mutation
  setBasket(basket: any) {
    this.basket = {...basket}
  }

  @Mutation
  setIncrementedItemToBasket(id: number) {
    if (typeof this.basket[id] === 'undefined') {
      this.basket[id] = 1
    } else {
      this.basket[id]++
    }
    this.basket = {...this.basket}

    localStorage.setItem("basket", JSON.stringify(this.basket))

  }

  @Mutation
  setDecrementedItemFromBasket(id: number) {
    if (this.basket[id] === 1) {
      delete this.basket[id]
    } else {
      this.basket[id]--
    }
    this.basket = {...this.basket}

    localStorage.setItem("basket", JSON.stringify(this.basket))
  }


  @Action
  async loadItems() {
    const response = await fetch("https://fakestoreapi.com/products")
    const items = await response.json()
    const filteredItems = items.filter((item: any) => {
      if (typeof item.title === 'undefined' || typeof item.price === 'undefined') {
        return false
      }
      return true
    })
    const formatedItems = Object.fromEntries(filteredItems.map((item: any) => {
      const id = Number(item.id)
      delete item.id
      return [id, item];
    }))

    this.setItems(formatedItems)
    return formatedItems
  }

  @Action
  async loadBasket(storage: any) {
    let basket = JSON.parse(storage)
    console.log(basket)
    this.setBasket(basket)
    return basket
  }
}

import {store} from "./index";

export const userModule = new UserModule({store, name: "user"});
